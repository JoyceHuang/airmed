<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE rdf:RDF [
    <!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#" >
    <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#" >
    <!ENTITY owl "http://www.w3.org/2002/07/owl#" >
    <!ENTITY xsd "http://www.w3.org/2001/XMLSchema#" >
    <!ENTITY dct "http://purl.org/dc/terms/" >
    <!ENTITY skos "http://www.w3.org/2004/02/skos/core#" >
    <!ENTITY sm "http://www.omg.org/techprocess/ab/SpecificationMetadata/" >
    <!ENTITY fibo-fnd-utl-av "http://spec.edmcouncil.org/fibo/FND/Utilities/AnnotationVocabulary/" >
    <!ENTITY fibo-fnd-rel-rel "http://spec.edmcouncil.org/fibo/FND/Relations/Relations/" >
    <!ENTITY fibo-fnd-arr-arr "http://spec.edmcouncil.org/fibo/FND/Arrangements/Arrangements/" >
    <!ENTITY fibo-fnd-arr-cd "http://spec.edmcouncil.org/fibo/FND/Arrangements/Codes/" >
]>

<rdf:RDF xml:base="http://spec.edmcouncil.org/fibo/FND/Arrangements/Codes/"
     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
     xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
     xmlns:owl="http://www.w3.org/2002/07/owl#"
     xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
     xmlns:dct="http://purl.org/dc/terms/"
     xmlns:skos="http://www.w3.org/2004/02/skos/core#"
xmlns:sm="http://www.omg.org/techprocess/ab/SpecificationMetadata/"
     xmlns:fibo-fnd-utl-av="http://spec.edmcouncil.org/fibo/FND/Utilities/AnnotationVocabulary/"
     xmlns:fibo-fnd-rel-rel="http://spec.edmcouncil.org/fibo/FND/Relations/Relations/"
     xmlns:fibo-fnd-arr-arr="http://spec.edmcouncil.org/fibo/FND/Arrangements/Arrangements/"
     xmlns:fibo-fnd-arr-cd="http://spec.edmcouncil.org/fibo/FND/Arrangements/Codes/">


    <owl:Ontology rdf:about="http://spec.edmcouncil.org/fibo/FND/Arrangements/Codes/">
        <rdfs:label>Codes and Code Sets Ontology</rdfs:label>


    <!-- Curation and Rights Metadata for the FIBO FND Codes and Code Sets Ontology -->

        <sm:copyright rdf:datatype="&xsd;string">Copyright (c) 2014 EDM Council, Inc.
Copyright (c) 2014 Object Management Group, Inc.</sm:copyright>
        <dct:license rdf:datatype="&xsd;anyURI">http://www.omg.org/techprocess/ab/SpecificationMetadata/MITLicense</dct:license>


    <!-- Ontology/File-Level Metadata for the FIBO FND Codes and Code Sets Ontology -->

        <sm:filename rdf:datatype="&xsd;string">Codes.rdf</sm:filename>
        <sm:fileAbbreviation rdf:datatype="&xsd;string">fibo-fnd-arr-cd</sm:fileAbbreviation>
        <owl:versionIRI rdf:resource="http://spec.edmcouncil.org/fibo/FND/20141101/Arrangements/Codes/"/>
        <sm:fileAbstract rdf:datatype="&xsd;string">This ontology defines abstract concepts for representation of codes and coding schemes for use in other FIBO ontology elements.</sm:fileAbstract>

        <skos:changeNote rdf:datatype="&xsd;string">The http://spec.edmcouncil.org/fibo/FND/20141101/Arrangements/Codes.rdf version of this ontology was introduced as a part of the issue resolutions identified in the FIBO FND 1.0 FTF report and in http://spec.edmcouncil.org/fibo/FND/1.0/AboutFND-1.0/ in advance of the Long Beach meeting in December 2014.</skos:changeNote>

        <sm:dependsOn rdf:datatype="&xsd;anyURI">http://spec.edmcouncil.org/fibo/FND/Arrangements/Arrangements/</sm:dependsOn>
        <sm:dependsOn rdf:datatype="&xsd;anyURI">http://spec.edmcouncil.org/fibo/FND/Relations/Relations/</sm:dependsOn>
        <sm:dependsOn rdf:datatype="&xsd;anyURI">http://spec.edmcouncil.org/fibo/FND/Utilities/AnnotationVocabulary/</sm:dependsOn>

        <sm:contentLanguage rdf:datatype="&xsd;anyURI">http://www.omg.org/spec/ODM/</sm:contentLanguage>
        <sm:contentLanguage rdf:datatype="&xsd;anyURI">http://www.w3.org/standards/techs/owl#w3c_all</sm:contentLanguage>

        <rdfs:seeAlso rdf:datatype="&xsd;anyURI">http://spec.edmcouncil.org/fibo/AboutTheEDMC-FIBOFamily/</rdfs:seeAlso>
        <rdfs:seeAlso rdf:datatype="&xsd;anyURI">http://spec.edmcouncil.org/fibo/FND/AboutFND/</rdfs:seeAlso>
        <rdfs:seeAlso rdf:datatype="&xsd;anyURI">http://spec.edmcouncil.org/fibo/FND/Arrangements/AboutArrangements/</rdfs:seeAlso>

        <owl:imports rdf:resource="http://spec.edmcouncil.org/fibo/FND/Utilities/AnnotationVocabulary/"/>
        <owl:imports rdf:resource="http://spec.edmcouncil.org/fibo/FND/Arrangements/Arrangements/"/>
        <owl:imports rdf:resource="http://spec.edmcouncil.org/fibo/FND/Relations/Relations/"/>

    </owl:Ontology>


    <!-- 
    ///////////////////////////////////////////////////////////////////////////////////////
    //
    // Classes
    //
    ///////////////////////////////////////////////////////////////////////////////////////
     -->

    <owl:Class rdf:about="&fibo-fnd-arr-cd;CodeElement">
        <rdfs:label>code element</rdfs:label>
        <rdfs:subClassOf rdf:resource="&fibo-fnd-rel-rel;Reference"/>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="&fibo-fnd-rel-rel;isMemberOf"/>
                <owl:onClass rdf:resource="&fibo-fnd-arr-cd;CodeSet"/>
                <owl:qualifiedCardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:qualifiedCardinality>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="&fibo-fnd-rel-rel;denotes"/>
                <owl:cardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:cardinality>
            </owl:Restriction>
        </rdfs:subClassOf>
        <skos:definition rdf:datatype="&xsd;string">a sequence of characters, capable of identifying that with which it is associated for some purpose, within a specified context, i.e., a code set, according to a pre-established set of rules</skos:definition>
        <fibo-fnd-utl-av:adaptedFrom>ISO/IEC 11179-3 Information technology - Metadata registries (MDR) - Part 3: Registry metamodel and basic attributes, Third edition, 2013-02-15</fibo-fnd-utl-av:adaptedFrom>
    </owl:Class>

    <owl:Class rdf:about="&fibo-fnd-arr-cd;CodeSet">
        <rdfs:label>code set</rdfs:label>
        <rdfs:subClassOf rdf:resource="&fibo-fnd-arr-arr;Arrangement"/>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="&fibo-fnd-rel-rel;defines"/>
                <owl:allValuesFrom rdf:resource="&fibo-fnd-arr-cd;CodeElement"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="&fibo-fnd-rel-rel;comprises"/>
                <owl:allValuesFrom rdf:resource="&fibo-fnd-arr-cd;CodeElement"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <skos:definition rdf:datatype="&xsd;string">A system of valid symbols that substitute for specified values, e.g., alpha, numeric, symbols and/or combinations</skos:definition>
        <skos:altLabel rdf:datatype="&xsd;string">coding scheme</skos:altLabel>
        <fibo-fnd-utl-av:adaptedFrom>ISO/IEC 11179-3 Information technology - Metadata registries (MDR) - Part 3: Registry metamodel and basic attributes, Third edition, 2013-02-15</fibo-fnd-utl-av:adaptedFrom>
    </owl:Class>
    
</rdf:RDF>